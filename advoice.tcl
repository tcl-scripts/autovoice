#
# Auto Devoice 1.5.0
#
# Author: CrazyCat <crazycat@c-p-f.org>
# https://www.eggdrop.fr
# ircs://irc.zeolia.chat/eggdrop
#
# Auto voice users on join
# Auto devoice users after an idle delay
#
# Usage:
#	- Console:
#	 .chanset #channel advdelay XX
# With XX in minutes (set to 0 to disable on the channel)
#	 .chanset #channel +/-joinvoice
# to enable/disable voice on join
#   .chanset #channel advlines XX
# With XX the number of lines someone must say in channel to be voiced
# (will disable voice on join)
# - Message:
#	 You need to be known as channel operator in eggdrop
#	 or to have the operator status on the channel
#	 Help:
#		 /msg eggdrop delay help
#	 Enable:
#		 /msg eggdrop delay #channel on (use default value)
#		 /msg eggdrop delay #channel XX
#	 Disable:
#		 /msg eggdrop delay #channel off|0
#	 Turn on/off voice on join:
#		 /msg eggdrop voj #channel on|off
#   Set number of lines (will disable onjoin)
#      /msg eggdrop nol #channel XX
#
# N.B.: the advoice default setting is 0 (disabled)

# Changelog
# 1.5.0 : unbugged version
# 1.4 : added number of line before voicing
# 1.3 : added /msg management of voiceonjoin
# 1.2 : added flag voiceonjoin
# 1.1 : corrected some returns which blocked the log

# TODO
# Implement the ChanServ (de)voice 

namespace eval advoice {

   # Default idle time
   variable defdelay 30

   # May we deop inactive ops ?
   variable opasvoice 0

   # Number of lines the user must type
   # before he's voiced
   variable nol 1
   
   # (Not implemented) Use chanserv to deop ? If yes, fill with ChanServ nickname
   # This will force the progressive mode to be enabled (to avoid flood)
   variable chanserv ""
   
   # Progressive mode enabled: the script removes modes one per one
   # disabled: all modes are removed in one time
   variable progressive 0

   # Protected users
   variable protected "\[Gamer\] \[Guru\] $::botnick"

   ##############################
   #									  #
   # DO NOT EDIT ANYTHING BELOW #
   #									  #
   ##############################

   setudef int advdelay
   setudef int advlines
   setudef flag advjoin
   
   variable author "CrazyCat"
   variable versionNum "1.5.0"
   variable versionName "AutoDeVoice"

   bind join - * [namespace current]::avjoin
   bind time - * [namespace current]::check
   bind pubm - * [namespace current]::pub
   bind msg - "delay" [namespace current]::setdelay
   bind msg - "voj" [namespace current]::setvoj
   bind msg - "nol" [namespace current]::setnol

   variable unolines
   
   proc setdelay {nick uhost handle text} {
      set args [split $text]
      set chan [string tolower [lindex $args 0]]
      if {![string first $chan "#"] == -1 && $chan ne "help"} {
         putserv "PRIVMSG $nick :Please, type /msg $::botnick delay help"
         return 0
      }
      if { $chan eq "help"} {
         putserv "PRIVMSG $nick :\002/msg $::botnick delay #chan on\002 to turn the AutoDeVoice on (with default delay: [set [namespace current]::defdelay minutes)";
         putserv "PRIVMSG $nick :\002/msg $::botnick delay #chan off|0\002 to turn the AutoDeVoice off";
         putserv "PRIVMSG $nick :\002/msg $::botnick delay #chan XX\002 to turn the AutoDeVoice on with a delay of XX minutes (between 1 and 999)";
         putserv "PRIVMSG $nick :\002/msg $::botnick delay #chan\002 to see the status"
         putserv "PRIVMSG $nick :\002/msg $::botnick join #chan\002 <on|off>"
         putserv "PRIVMSG $nick :\002/msg $::botnick lines #chan\002 XX"
         return 0
      }
      if {![[namespace current]::isoperator $nick $handle $chan]} { return }
      set delay [lindex $args 1]
      switch -regexp -- $delay {
         ^on$ {
            channel set $chan advdelay [set [namespace current]::defdelay]
            channel set $chan advlines [set [namespace current]::nol]
            putserv "PRIVMSG $nick :Delay is now [channel get $chan advdelay] minutes"
         }
         ^off$ -
         ^0$ {
            channel set $chan advdelay 0
            putserv "PRIVMSG $nick :Ok, [set [namespace current]::versionName] disabled"
         }
         {^\d{1,3}$} {
            channel set $chan advdelay $delay
            putserv "PRIVMSG $nick :Delay is now [channel get $chan advdelay] minutes"
         }
         default {putserv "PRIVMSG $nick :The idle delay on $chan is actually [channel get $chan advdelay] minutes";}
      }
   }

   proc setvoj {nick uhost handle text} {
      set args [split $text]
      set chan [string tolower [lindex $args 0]]
      if {![string first $chan "#"] == -1 || $chan eq ""} {
         putserv "PRIVMSG $nick :Please, type /msg $::botnick voj #channel on|off"
         return 0
      }
      if {![[namespace current]::isoperator $nick $handle $chan]} { return }
      if {[channel get $chan advlines]>1} {
         putserv "PRIVMSG $nick :Can not set VoiceOnJoin because minimum lines is more than 1"
         return
      }
      set flag [lindex $args 1]
      switch -nocase -- $flag {
         on { channel set $chan +joinvoice }
         off { channel set $chan -joinvoice }
         default {
            putserv "PRIVMSG $nick :Please, type /msg $::botnick voj #channel on|off"
            return
         }
      }
      putserv "PRIVMSG $nick :Voice on join is now turned $flag in $chan"
   }
   
   proc setnol {nick uhost handle text} {
      set args [split $text]
      set chan [string tolower [lindex $args 0]]
      if {![string first $chan "#"] == -1 || $chan eq ""} {
         putserv "PRIVMSG $nick :Please, type /msg $::botnick nol #channel XX"
         return
      }
      if {![[namespace current]::isoperator $nick $handle $chan]} { return }
      set val [expr {[lindex $args 1] * 1}]
      if {$val <= 1} {
         channel set $chan advlines 1
      } else {
         channel set $chan advlines $val
         channel set $chan -advjoin
      }
      putserv "PRIVMSG $nick :Required number of lines on $chan is now [channel get $chan advlines]"
   }

   proc isoperator {nick handle chan} {
      if {![validchan $chan]} {
         putserv "PRIVMSG $nick :Sorry, I'm not on $chan"
         return 0
      }
      if {![isop $nick $chan] && ![wasop $nick $chan] && ![matchattr $handle o $chan]} {
         putserv "PRIVMSG $nick :Sorry, you're not operator (@) on $chan"
         return 0
      }
      return 1
   }

   proc avjoin {nick uhost handle chan} {
      if {![channel get $chan advjoin]} {
         return
      }
      if {[channel get $chan advlines]>1} {
         return
      }
      if {[info exists [namespace current]::unolines($chan,$nick)]} {
         set [namespace current]::unolines($chan,$nick) 0
      }
      if {[lsearch -nocase [[namespace current]::filt [split [set [namespace current]::protected]]] [[namespace current]::filt [split $nick]]] >= 0} {
         return
      }
      if {[channel get $chan advdelay]==0} {
         return
      }
      after 5000 {set delayed ok}
      vwait delayed
      if {![isop $nick $chan] && ![ishalfop $nick $chan]} {
         [namespace current]::voice $nick $chan
      }
   }

   proc check {min hour day month year} {
      foreach chan [channels] {
         set idlemax [channel get $chan advdelay]
         if {$idlemax==0} { continue }
         foreach nick [chanlist $chan] {
            if {[lsearch -nocase [split [[namespace current]::filt [set [namespace current]::protected]]] [[namespace current]::filt [split $nick]]] >= 0} { continue }
            if {[getchanidle $nick $chan] > $idlemax} {
               if {[info exists [namespace current]::unolines($chan,$nick)] && [set [namespace current]::unolines($chan,$nick)]>0} {
                  incr [namespace current]::unolines($chan,$nick) -1
               }
               if {[isop $nick $chan]} {
                  if {[set [namespace current]::opasvoice] == 0} {
                     continue
                  } else {
                     pushmode $chan -o $nick
                     [namespace current]::csdeop $nick $chan
                     if {[set [namespace current]::progressive] == 1 || [set [namespace current]::chanserv] ne ""} { continue }
                  }
               }
               if {[ishalfop $nick $chan]} {
                  if {[set [namespace current]::opasvoice] == 0} {
                     continue
                  } else {
                     pushmode $chan -h $nick
                     [namespace current]::csdehalfop $nick $chan
                     if {[set [namespace current]::progressive] == 1 || [set [namespace current]::chanserv] ne ""} { continue }
                  }
               }
               if {![isvoice $nick $chan]} { continue }
               pushmode $chan -v $nick
               [namespace current]::csdevoice $nick $chan
               if {[set [namespace current]::progressive] == 1 || [set [namespace current]::chanserv] ne ""} { continue }
            }
         }
         flushmode $chan
      }
   }

   proc pub {nick uhost handle chan text} {
      [namespace current]::voice $nick $chan
   }
   
   proc voice {nick chan} {
      if {[channel get $chan advdelay]==0} { return }
      if {[lsearch -nocase [split [[namespace current]::filt [set [namespace current]::protected]]] [[namespace current]::filt [split $nick]]] >= 0} { return }
      if {[isop $nick $chan]} { return }
      if {[ishalfop $nick $chan]} { return }
      incr [namespace current]::unolines($chan,$nick)
      if {![isvoice $nick $chan] && ![[namespace current]::csvoice $nick $chan] && [channel get $chan advlines]<=[set [namespace current]::unolines($chan,$nick)]} {
         pushmode $chan +v $nick
      }
   }

   proc csdeop {nick chan} {
      if {[set [namespace current]::chanserv] ne ""} {
         putserv "PRIVMSG [set [namespace current]::chanserv] :deop $chan $nick"
         return 1
      }
      return 0
   }

   proc csdehalfop {nick chan} {
      if {[set [namespace current]::chanserv] ne ""} {
         putserv "PRIVMSG [set [namespace current]::chanserv] :dehalfop $chan $nick"
         return 1
      }
      return 0
   }

   proc csvoice {nick chan} {
      if {[set [namespace current]::chanserv] ne ""} {
         putserv "PRIVMSG [set [namespace current]::chanserv] :voice $chan $nick"
         return 1
      }
      return 0
   }

   proc csdevoice {nick chan} {
      if {[set [namespace current]::chanserv ne ""} {
         putserv "PRIVMSG [set [namespace current]::chanserv] :devoice $chan $nick"
         return 1
      }
      return 0
   }

   proc filt {data} {
      return [regsub -all {\W} $data {\\&}]
   }

   putlog "[set [namespace current]::versionName] [set [namespace current]::versionNum] [set [namespace current]::author] loaded"
}
